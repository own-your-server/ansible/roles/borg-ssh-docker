# Borg SSH Docker

Serveur SSH avec borg installé pour permettre des sauvegardes distante d'un serveur utilisant borg.

## Installation

### Dépendances
* [ansible docker host](https://framagit.org/cycles-manivelles/ansible-role_docker-host)

### variables
Les variables sont visibles dans `var/defaults`

nécessaires:

* **ssh_user_name** (défaut: backup) => utilisateur pour la connexion ssh
* **ssh_port** (défaut: 2222) => Port ouvert vers l'extérieur pour la connexion
* **ssh_authorized_keys** => Chemin vers les clés autorisées. Se trouve normalement dans l'inventorie. Sans ça il n'est pas possible de se connecter au serveur ssh.

secondaires:
* **ssh_user_pid** => par défaut 1500, peut rester ainsi, l'idée est d'avoir un PID qui n'est pas dans le système hote pour éviter qu'un attaquant puisse avoir des droits en cas d'escalation.*
* **ssh_group_pid**: idem que ssh_user_pid, garde la même valeur par défaut
* **ssh_pod_name** => Nom du dossier, par défaut ce sera le nom d'utilisateur 
* **root_directory** (défaut: /srv) => base du chemin dans lequel les fichiers vont être déposés
* **compose_directory**: (défaut: "{{ root_directory }}/docker-compose/{{ ssh_pod_name }}-ssh") chemins où le reste des fichiers sont déposés.

### récupérer les backups

Les sauvegardes sont dans un volume monté dans le répertoire docker. Vous pouvez donc y accéder en allant dans le répertoire "compose_directory" et en ouvrant le dossier `data`.

Normalement votre client doit avoir chiffré la sauvegarde, il faut donc penser à récupérer la clé dans votre inventory.
